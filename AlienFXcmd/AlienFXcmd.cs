﻿using LightFX;
using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace AWAlienFXcmd {
    class AlienFXcmd {

        static LightFXController lightFX = new LightFXController();
        static LFX_Result result;
        static bool initialized = false;

        // delay to give update method time to apply changes
        static int delay = 100;

        static Dictionary<uint, Light> lights = new Dictionary<uint, Light>();

        class Light {
            public uint lightID;
            public LFX_ColorStruct color;

            public Light(uint _lightID, LFX_ColorStruct _color) {
                lightID = _lightID;
                color = _color;
            }
        }

        static void Main(string[] args) {

            uint lightID;
            uint deviceID = 0;

            // if no arguments - show help
            if(args.Length == 0) ShowHelp();

            // "priority" arguments
            for(int i = 0; i < args.Length; i++) {
                if((new[] { "--help", "-h", "/?" }).Contains(args[i], StringComparer.OrdinalIgnoreCase)) {
                    ShowHelp();
                    break;
                }
                if((new[] { "--info", "-i" }).Contains(args[i], StringComparer.OrdinalIgnoreCase)) {
                    LightFXInit();
                    LightsInfo();
                    break;
                }
                if((new[] { "--release", "-r" }).Contains(args[i], StringComparer.OrdinalIgnoreCase)) {
                    LightFXInit();
                    if(lightFX.LFX_Release() == LFX_Result.LFX_Success)
                        Console.WriteLine("  Alien FX system released.");
                    System.Threading.Thread.Sleep(delay);
                    break;
                }
                if((new[] { "--device", "-d"}).Contains(args[i], StringComparer.OrdinalIgnoreCase)) {
                    if(i + 1 < args.Length) {
                        uint.TryParse(args[i].Substring(1), out deviceID);
                    }
                }
            }

            // lights arguments
            uint numLights = 0;
            byte[] colors;
            byte clr;
            for(int i = 0; i < args.Length; i++) {

                uint.TryParse(args[i].Substring(1), out lightID);
                if(lightID >= 0 && i+1 < args.Length) {

                    // init LightFX and get number of lights
                    if(!initialized) {
                        LightFXInit();
                        lightFX.LFX_GetNumLights(deviceID, out numLights);
                    }

                    // parse colors
                    string[] clrs = args[i + 1].Split(',');
                    bool error = false;
                    colors = new byte[4];
                    for(int j = 0; j < clrs.Length; j++) {
                        if(!byte.TryParse(clrs[j], out clr)) {
                            error = true;
                            Console.WriteLine("Error: color for light index " + lightID + " isn't specified properly. Should be in b,r,g,b format, i.e.: 255,255,0,0");
                            break;
                        }
                        colors[j] = clr;
                    }

                    i++;
                    if(error) continue;
                    if(lightID >= numLights) {
                        Console.WriteLine("Error: light index " + lightID + " isn't correct. Max index: " + (numLights-1));
                        continue;
                    }

                    lights.Add(lightID, new Light(lightID, new LFX_ColorStruct(colors[0], colors[1], colors[2], colors[3])));
                }
            }

            // set lights
            if(lights.Count > 0)
                SetLights(lights, deviceID);

            Environment.Exit(0);
        }

        // TODO: lights animations (colours morphing) using LFX_SetLightActionColor
        // set zones lights and log some info
        private static void SetLights(Dictionary<uint, Light> lights, uint deviceID) {
            lightFX.LFX_Reset();
            lightFX.LFX_Update();
            System.Threading.Thread.Sleep(delay);

            StringBuilder description;
            for(byte i = 0; i < lights.Count; i++) {
                lightFX.LFX_SetLightColor(deviceID, lights.ElementAt(i).Value.lightID, lights.ElementAt(i).Value.color);

                lightFX.LFX_GetLightDescription(deviceID, lights.ElementAt(i).Value.lightID, out description, 255);
                Console.WriteLine(string.Format("  Light: {0, -10} Description: {1, -30} Color: {2, -25}", 
                    lights.ElementAt(i).Key, description, lights.ElementAt(i).Value.color));
            }
            
            if(lightFX.LFX_Update() == LFX_Result.LFX_Success)
                Console.WriteLine("\n  Done.");

            System.Threading.Thread.Sleep(delay);
        }

        // LightFX initialization
        private static void LightFXInit() {
            if(initialized)
                return;
            result = lightFX.LFX_Initialize();
            if(result == LFX_Result.LFX_Success) {
                initialized = true;
            } else {
                switch(result) {
                    case LFX_Result.LFX_Error_NoDevs:
                        Console.WriteLine("There is no AlienFX device available.");
                        break;
                    default:
                        Console.WriteLine("There was an error initializing the AlienFX device. 3rd Party AlienFX Access should be enabled.");
                        break;
                }
                Environment.Exit(0);
            }
        }

        // show list of available lights
        private static void LightsInfo() {
            
            uint numDevices;
            lightFX.LFX_GetNumDevices(out numDevices);
            Console.WriteLine(string.Format("Devices: {0}", numDevices));
            
            for(uint devIndex = 0; devIndex < numDevices; devIndex++) {
                LFX_DeviceType devType;
                StringBuilder description;
                result = lightFX.LFX_GetDeviceDescription(devIndex, out description, 255, out devType);
                if(result != LFX_Result.LFX_Success)
                    continue;

                Console.WriteLine(string.Format("Device {0}: Description: {1} Type: {2}", devIndex, description, devType));

                uint numLights;
                result = lightFX.LFX_GetNumLights(devIndex, out numLights);
                if(result != LFX_Result.LFX_Success)
                    continue;

                Console.WriteLine(string.Format("  {0, -15} {1, -25} {2, -30}", "Light pos: ", "Description: ", "Location: "));
                for(uint lightIndex = 0; lightIndex < numLights; lightIndex++) {
                    result = lightFX.LFX_GetLightDescription(devIndex, lightIndex, out description, 255);
                    if(result != LFX_Result.LFX_Success)
                        continue;

                    LFX_Position location;
                    result = lightFX.LFX_GetLightLocation(devIndex, lightIndex, out location);
                    if(result != LFX_Result.LFX_Success)
                        continue;

                    Console.WriteLine(string.Format("  {0, -15} {1, -25} {2, -30}", lightIndex, description, location));
                }
            }
        }

        // self explanatory
        private static void ShowHelp() {
            Console.WriteLine(
@"AlienFXcmd.exe

  --info, -i        list of devices and lights
  --device, -d      index of device to use (Default: 0)
  -n b,r,g,b        where:
                     n - light index
                     b - brightness
                     r - red 
                     g - green
                     b - blue
  --release, -r     release Alienware AlienFX system, freeing memory 
                    and restoring the state machine
  --help, -h, /?    this help

  3rd Party AlienFX Access should be enabled.

  Example: AlienFXcmd.exe -0 255,255,0,0 -1 255,255,0,255 -2 255,0,0,255
");
            if(!Console.IsOutputRedirected) {
                Console.WriteLine("  Press any key for more info, Escape to exit...");
                var key = Console.ReadKey(true);
                if(key.Key == ConsoleKey.Escape)
                    Environment.Exit(0);
            }

            Console.WriteLine();
            LightFXInit();
            LightsInfo();

            //Console.WriteLine("\n  All light positions:");
            //foreach(LFX_Position value in Enum.GetValues(typeof(LFX_Position)))
            //    Console.WriteLine("    {0, -25}: {1}", ((LFX_Position)value), (int)value);
        }
    }
}
