# AlienFXcmd

Win32 build: [Download](https://mega.nz/#!4F9kWbwB!tYA-df5ZHAyTMsPwhPvyEefaXXXt5hswXVl4zO0uTIs)

Command line arguments:

    --info, -i       	list of devices and lights
    --device, -d		index of device to use (Default: 0)
    -n b,r,g,b			where:
	                     n - light index
	                     b - brightness
	                     r - red 
	                     g - green
	                     b - blue
    --release, -r     release Alienware AlienFX system, freeing memory 
	                    and restoring the state machine
    --help, -h, /?    this help

    3rd Party AlienFX Access should be enabled.

    Example: AlienFXcmd.exe -0 255,255,0,0 -1 255,255,0,255 -2 255,0,0,255
